import sys

path = sys.argv[1]
name = sys.argv[2]

from allennlp.common.util import import_module_and_submodules
import_module_and_submodules('machamp')
from allennlp.models import load_archive
archive = load_archive(path)
from allennlp.common import push_to_hf
model = archive.model._text_field_embedder.token_embedder_tokens._matched_embedder.transformer_model
#model.save_pretrained('outDir')

from allennlp.data.tokenizers import Tokenizer, PretrainedTransformerTokenizer
tokenizer: PretrainedTransformerTokenizer = Tokenizer.from_params(archive.config['dataset_reader']['tokenizer'])
#tokenizer.tokenizer.save_pretrained('outDir')

model.push_to_hub(name, use_auth_token=True)
tokenizer.tokenizer.push_to_hub(name, use_auth_token=True)

